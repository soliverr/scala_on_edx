/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import akka.event.LoggingReceive

import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection */
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor with ActorLogging {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  // This is the Root of an instance of the BinTree
  var root = createRoot

  // optional (used to stash incoming operations during garbage collection)
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
//  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = LoggingReceive {
    // Pass current operation to current Root actor
    case op : Operation => 
      log.info(s"normal context: Get Operation ${op.getClass} message")
      root ! op        
    //сase op : Operation => root.forward(op)
    case GC =>
      log.info(s"normalcontext: Get GC message")
      // Garbage collector creates new BinTree
      val newRoot = createRoot                    // Create new root for current BinTree
      context.become(garbageCollecting(newRoot))  // Build new version of current BinTree
      root ! CopyTo(newRoot)                      // Send message request to rebuild the BinTree
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = LoggingReceive {
    case op : Operation => 
      // Store current Operation while preforming GC
      log.debug(s"GC context: Queue operations ${op.getClass}")
      pendingQueue = pendingQueue.enqueue(op)
    case CopyFinished => 
      // New tree is ready. Start normal processing
      log.info(s"GC context: Get CopyFinished")
      
      /*
      pendingQueue.foreach { newRoot ! _ }
      pendingQueue = Queue.empty[Operation]
      root = newRoot
      context.unbecome()
      */
      root ! PoisonPill // Kill previous actor of the current BinTree
      root = newRoot    // This is a new actor/BinTree
      // Processing queue and fresh coming operations in GC actor context
      pendingQueue.foreach { root ! _ }
      /*
      while (!pendingQueue.isEmpty) {
        root ! pendingQueue.dequeue()
      }
      */
      /*
      while (pendingQueue.nonEmpty) {
        val (op, remaining) = pendingQueue.dequeue
        println(s"Finishing ${op}")
        root ! op
        pendingQueue = remaining
      }
      */
      // Queue should be empty in the end
      // https://courses.edx.org/courses/course-v1:EPFLx+scala-reactiveX+3T2019/discussion/forum/i4x-EPFLx-ScalaReactiveX-2019t3-03/threads/5e77cadccd56ad08f69e7a89
      pendingQueue = Queue.empty[Operation]
      // Switch to normal processing context
      context.become(normal)  
      //context.unbecome()
    case GC  =>  // Do nothing, 'cause already in GC
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  /**
   * Acknowledges that a copy has been completed. This message should be sent
   * from a node to its parent, when this node and all its children nodes have
   * finished being copied.
   */
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

/* It's seems that initiallyRemoved used in educational purpose only.
   This flag mark root node of BinaryTree as removed for CopyTo message of garbage collector

   https://courses.edx.org/courses/course-v1:EPFLx+scala-reactiveX+3T2019/discussion/forum/i4x-EPFLx-ScalaReactiveX-2019t3-03/threads/5e10bbe395b06209410033cf:

   > The only way to model an empty tree with the proposed model is indeed to have a single (root) node marked as “removed” 
   > so that `Contains` query don’t return `true`, and the root node is not copied on `CopyTo` operations.
 */
class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with ActorLogging {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // Get position for new element of BinaryTree
  private def getPosition(e: Int): Position = {
    if (e > elem) Right
    else Left
  }

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = LoggingReceive {
    //case op : Insert =>
    // We can redefine message's parameters names here for convenience
    case Insert(op_sender, op_id, new_elem) =>
      // Process insertions messages
      log.debug(s"Get Insert message: Op_Id = ${op_id} Element = ${new_elem}")
      if (new_elem == elem) {
        // Current actor consists of this element, update removed flag
        removed = false
        op_sender ! OperationFinished(op_id)
      } else {
        // Walk through tree: find actor for this element
        val next = getPosition(new_elem)
        // if (subtrees.contains(next)) subtrees(next) ! op
        if (subtrees.isDefinedAt(next)) subtrees(next) ! Insert(op_sender, op_id, new_elem)
        else {
          // Insert new actor/element in BinaryTree
          subtrees += (next -> context.actorOf(props(new_elem, false)))
          op_sender ! OperationFinished(op_id)
        }
      }
    case Contains(op_sender, op_id, new_elem) =>
      // Process check messages
      log.debug(s"Get Contains message: Op_Id = ${op_id} Element = ${new_elem}")
      if (new_elem == elem) op_sender ! ContainsResult(op_id, !removed) // Element found, but check removed flag
      else {
        // Walk through tree
        val next = getPosition(new_elem)
        if(subtrees.isDefinedAt(next)) subtrees(next) ! Contains(op_sender, op_id, new_elem)
        else op_sender ! ContainsResult(op_id, false) // Element not found
      }
    case Remove(op_sender, op_id, new_elem) =>
      // Process remove message, ignore initialRemoved flag
      log.debug(s"Get Remove message: Op_Id = ${op_id} Element = ${new_elem}")
      if (new_elem == elem) {
        removed = true
        op_sender ! OperationFinished(op_id)
      }
      else {
        // Walk through tree
        val next = getPosition(new_elem)
        if(subtrees.isDefinedAt(next)) subtrees(next) ! Remove(op_sender, op_id, new_elem)
        else op_sender ! OperationFinished(op_id) // Element not found
      }
    /*
    case op @ CopyTo(new_root) =>
      // Get children of current node
      val children = Set(subtrees.get(Left), subtrees.get(Right)).flatten

      if (removed && children.isEmpty) {
        // Last node - root node was set up as removed !!!
        context.parent ! CopyFinished
        context.stop(self)
      } else {
        // switch to copying context for all children
        context.become(copying(children, removed))
        if (!removed) {
          new_root ! Insert(self, 0, elem)  // copy node by sending Insert message. Op_id is -1 is not traced
        }
        children.foreach { _ ! op } // walk through children
      }
      */
      /*
      case c: CopyTo =>
        if (!removed) c.treeNode ! Insert(self, elem, elem)

        var s = Set[ActorRef]()

        if (subtrees.contains(Right)) {
          subtrees(Right) ! c
          s = s + subtrees(Right)
        }
        if (subtrees.contains(Left)) {
          subtrees(Left) ! c
          s = s + subtrees(Left)
        }

        if (s.isEmpty && removed)
          context.parent ! CopyFinished
        else
          context.become(copying(s, removed))
      */
      case CopyTo(new_root) =>
        log.info(s"Get CopyTo message")
        val children = Set(subtrees.get(Left), subtrees.get(Right)).flatten

        if (removed && children.isEmpty) {
          // Last node - root node was set up as removed !!!
          log.info(s"CopyTo: Last node")
          context.parent ! CopyFinished
          //context.stop(self)
          self ! PoisonPill
        } else {
          // switch to copying context for all children
          log.info(s"CopyTo: Switch to Copying for ${children.size} children")
          context.become(copying(children, removed))
          if (!removed) {
            new_root ! Insert(self, -1, elem)  // copy node by sending Insert message. Op_id is -1 is not traced
          }
          children.foreach { _ ! CopyTo(new_root) } // walk through children
        }
      /*
      case CopyTo(new_root) =>
        val children = subtrees.values.toSet
        if (!removed) treeNode ! Insert(self, -1 /* self insert */ , elem)
        children.foreach {
          _ ! CopyTo(treeNode)
        }
        isCopyDone(children, removed)  
      */  
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    *
    * https://courses.edx.org/courses/course-v1:EPFLx+scala-reactiveX+3T2019/discussion/forum/i4x-EPFLx-ScalaReactiveX-2019t3-03/threads/5e0d06d3c762770920002a1f
    *
    * > The `OperationFinished` reply will not come from the `target` actor, but from another actor representing the internal node where elem is saved. 
    * > Since there is no way to know which actor will that be, it is not possible to add to `expected` set before hand and so we are using a separate flag for it.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = LoggingReceive {
    case OperationFinished(id) =>
      log.info(s"Copying: Operation ${id} finished")
      if (expected.isEmpty) {
        // No more children
        context.parent ! CopyFinished
        self ! PoisonPill
        //context.stop(self)
      } else {
        context.become(copying(expected, true))
      }
    case CopyFinished =>
      // Remove processed children from `expected` set
      val newExpected = expected - sender
      log.info(s"Copying: CopyFinished. Remain ${newExpected.size} children to copy")
      if (newExpected.isEmpty && insertConfirmed) {
        context.parent ! CopyFinished
        self ! PoisonPill
        //context.stop(self)
      } else {
        context.become(copying(newExpected, insertConfirmed))
      }
    /* 
    case _: OperationFinished =>
      if (expected.isEmpty) context.parent ! CopyFinished
      else context.become(copying(expected, true))
    case CopyFinished =>
      val e = expected - sender
      if (!e.isEmpty || !insertConfirmed) context.become(copying(e, insertConfirmed))
      else context.parent ! CopyFinished
      sender ! PoisonPill
    */
    /*
    case OperationFinished(-1) => isCopyDone(expected, insertConfirmed = true)
    case CopyFinished => isCopyDone(expected - sender, insertConfirmed)  
    */
  }

  /*
  private def isCopyDone(expected: Set[ActorRef], insertConfirmed: Boolean): Unit = {
    if(expected.isEmpty && insertConfirmed) self ! PoisonPill
    else context.become(copying(expected, insertConfirmed))
  }

  // Stop Hook for Akka Actor
  override def postStop(): Unit = context.parent ! CopyFinished
  */
}
