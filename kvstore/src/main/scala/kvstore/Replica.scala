package kvstore

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, OneForOneStrategy, PoisonPill, Props, SupervisorStrategy, Terminated}
import akka.event.LoggingReceive
import kvstore.Arbiter._
import akka.pattern.{ask, pipe}

import scala.concurrent.duration._
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  // Enhanced control of client protocol
  case class OperationTimeout(id: Long, client: ActorRef)

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with ActorLogging {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // Storage
  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  // Replication protocol
  // a sequence number synchronizer
  var seqExpected = 0L

  // Persistence protocol (do it like Replicator)
  val persistence = context.actorOf(persistenceProps)
  // map from sequence number to tuple of sender, Persist, persist timer, operation timer
  var persistenceLeaderAcks = Map.empty[Long, (ActorRef, Persist, Cancellable, Cancellable)]
  // map from sequence number to tuple of sender, Persist, persist timer
  var persistenceSecondaryAcks = Map.empty[Long, (ActorRef, Persist, Cancellable)]
  // map to control replication protocol in primary: sender, key, replicators count, Operation timer
  var replicatorAcks = Map.empty[Long, (ActorRef, String, Long, Cancellable)]
  // Restart in case of persistence failure
  override val supervisorStrategy: OneForOneStrategy = OneForOneStrategy() {
    case _: PersistenceException => Restart
  }

  // On startup join to Arbiter
  override def preStart(): Unit = {
    log.info("REPLICA: Joining to Arbiter {}", arbiter)
    arbiter ! Join
  }

  def receive = {
    case JoinedPrimary   => {
      log.info("REPLICA: Joined as PRIMARY")
      context.become(leader)
    }
    case JoinedSecondary => {
      log.info("REPLICA: Joined as SECONDARY")
      context.become(replica)
    }
  }

  /* Behavior for  the leader role. */
  val leader: Receive = LoggingReceive {
    // Got from Arbiter all known Replicas
    case Replicas(replicas) => {
      /* Also, notice that at creation time of the Replicator, the primary must forward update
         events for every key-value pair it currently holds to this Replicator.
       */
      log.info("REPLICA leader: Count of input replicas: {}", replicas.size)
      log.info("REPLICA leader: Count of current replicators: {}", replicators.size)
      log.info("REPLICA leader: Count of current secondaries replica: {}", secondaries.size)

      // Calculate difference of Secondary Replicas
      val nowReplicas = replicas - self
      val bornReplicas = nowReplicas.filterNot(secondaries.keySet)
      val deadReplicas = secondaries.keySet.filterNot(nowReplicas)

      log.info("REPLICA: leader: Input replicas: {}", nowReplicas)
      log.info("REPLICA leader: Born replicas: {}", bornReplicas)
      log.info("REPLICA leader: Dead replicas: {}", deadReplicas)

      // Stop all Replicators for dead Replicas
      for (r <- deadReplicas) {
        val rep = secondaries(r)
        //rep ! PoisonPill
        context.stop(rep)
        replicators -= rep
        secondaries -= r
        // Imitate successful Replication
        replicatorAcks.foreach {
          case (id, (_, key, _, _)) =>
            log.info("REPLICA leader: Imitate successful replication: id={}, key={}", id, key)
            self ! Replicated(key, id)
          case _ =>
        }
      }

      // Add new Replicators for born Replicas
      for(r <- bornReplicas) {
        val rep = context.system.actorOf(Replicator.props(r))
        log.info("REPLICA leader: Replicas: Create replicator {} for replica {}", rep, r)
        replicators += rep
        secondaries += ((r, rep))
      }

      // Replicate to new born Replicas
      if (bornReplicas.size > 0) {
        val id = 0L
        var cnt = 0L
        bornReplicas.foreach { replicas =>
          // Get Replicator
          val rep = secondaries(replicas)
          // Send data to new Replicas through Replicator
          for ((k, v) <- kv) {
            log.info("REPLICA leader: Replicas: Start Replicate(id={}) key={} value={} to {}", id, k, v, rep)
            rep ! Replicate(k, Option(v), id)
            cnt += 1
          }
          if (cnt > 0) {
            log.info("REPLICA leader: Replicas: Initiated replication to {} secondaries", cnt)
            // TODO: Control operation of initial replication. How to distinguish common replication and initial replication
            // Start operation timer to control client protocol
            //val opTimer = context.system.scheduler.scheduleOnce(1.second, self, OperationTimeout(id, sender))
            //replicatorAcks += ((id, (self, bornReplicas.size, opTimer)))
          }
        }
      }
    }
    case Insert(key, value, id) => {
      log.info("REPLICA leader: Insert(id={}): key = {} value = {}", id, key, value)
      // Add to local index
      kv += ((key, value))

      // Persistence magic
      log.info("REPLICA leader: Insert(id={}): Persisting key = {} value = {}", id, key, value)
      val timer = doPersist(key, Option(value), id)

      // Start operation timer to control client protocol
      val opTimer = context.system.scheduler.scheduleOnce(1.second, self, OperationTimeout(id, sender))

      // Keep current operation and timer to control persistence protocol
      persistenceLeaderAcks += ((id, (sender, Persist(key, Option(value), id), timer, opTimer)))

      // Keep replication operation and timer to control replication protocol
      if (replicators.size > 0){
        log.info("REPLICA leader: Insert(id={}): Replicating key = {} to {} replicators", id, key, replicators.size)

        // Replication magic
        replicators.foreach { _ ! Replicate(key, Option(value), id) }
        replicatorAcks += ((id, (sender, key, replicators.size, opTimer)))
      }
    }
    case Remove(key, id) => {
      log.info("REPLICA leader: Remove(id={}): key = {}", id, key)
      // Remove from local index
      kv -= key

      // Replication magic
      replicators.foreach { _ ! Replicate(key, Option.empty, id)}

      // Persistence magic
      log.info("REPLICA leader: Remove(id={}): Persisting key = {}", id, key)
      val timer = doPersist(key, Option.empty, id)

      // Start operation timer to control client protocol
      val opTimer = context.system.scheduler.scheduleOnce(1.second, self, OperationTimeout(id, sender))

      // Keep current operation and timer to control persistence protocol
      persistenceLeaderAcks += ((id, (sender, Persist(key, Option.empty, id), timer, opTimer)))

      // Keep replication operation and timer to control replication protocol
      if (replicators.size > 0){
        replicatorAcks += ((id, (sender, key, replicators.size, opTimer)))
        log.info("REPLICA leader: Remove(id={}): Replicating key = {} to {} replicators", id, key, replicators.size)
      }
    }
    case Get(key, id) => {
      log.info("REPLICA leader: Get(id={}): key = {} value={}", id, key, kv.get(key))
      sender ! GetResult(key, kv.get(key), id)
    }
    case OperationTimeout(id, client) => {
      log.info("REPLICA leader: OperationTimeout(id={})", id)
      persistenceLeaderAcks.get(id) match {
        case Some((s, _, t, opTimer)) =>
          log.info("REPLICA leader: OperationFail(id={}) to client {}", id, client)
          // Stop timers
          t.cancel
          opTimer.cancel
          client ! OperationFailed(id)
          persistenceLeaderAcks -= id
        case _ =>
          log.info("REPLICA leader: OperationTimeout(id={}): Persisted, but not replicated", id)
          client ! OperationFailed(id)
      }
    }
    case PersistTimeout(id) => {
      log.info("REPLICA leader: PersistTimeout(id={}).", id)
      persistenceLeaderAcks.get(id) match {
        case Some((s, p, t, opTimer)) =>
          log.info("REPLICA leader: Resend Persist(id={})", id)
          // Stop timer
          t.cancel
          val timer = doPersist(p.key, p.valueOption, id)
          // Keep current operation and timer to control replication protocol
          persistenceLeaderAcks -= id
          persistenceLeaderAcks += ((id, (s, Persist(p.key, p.valueOption, id), timer, opTimer)))
        case _ => log.info("REPLICA leader: Race condition. Persist(id={}) already processed", id)
      }
    }
    case Persisted(key, id) => {
      // Clean persistence map, control replication protocol
      log.info("REPLICA leader: Persisted(id={}): key={}", id, key)
      persistenceLeaderAcks.get(id) match {
        case Some((s, _, t, opTimer)) =>
          // Stop timer
          t.cancel
          // Check is all replication is finished
          if (!replicatorAcks.contains(id)) {
            // No Replicators running: Send finish to sender
            // Stop timer
            opTimer.cancel
            s ! OperationAck(id)
            log.info("REPLICA leader: Persisted! OperationAck(id={}). key={} client={}", id, key, s)
          }
          else {
            val (_, _, c, _) = replicatorAcks.getOrElse(id, (null, null, null, null))
            log.info("REPLICA leader: Persisted(id={}): key={}: Keep waiting while all {} Replicators stops",
              id, key, c)
          }
          persistenceLeaderAcks -= id
        case _ => log.info("REPLICA leader: Race condition: id={}", id)
      }
    }
    case Replicated(key, id) => {
      // Cleanup replicators map, control replication protocol
      log.info("REPLICA leader: Replicated(id={}): key={}", id, key)
      replicatorAcks.get(id) match {
        case Some((s, _, c, opTimer)) =>
          var cnt = c
          cnt -= 1
          if (cnt <= 0) {
            log.info("REPLICA leader: Operation(id={}): All Replicator processed: key={}", id, key)
            // Check if no persistence running
            if (!persistenceLeaderAcks.contains(id)) {
              // No Persistence running
              // Stop timer
              opTimer.cancel
              s ! OperationAck(id)
              log.info("REPLICA leader: Replicated! OperationAck(id={}): key={} to client={}", id, key, s)
            }
            else {
              log.info("REPLICA leader: Replicated(id={}): key={}: Keep waiting while Persistence is finishing", id, key)
            }
            replicatorAcks -= id
          }
          else {
            log.info("REPLICA leader: Operation(id={}): Still waiting for {} Replicator: key = {}", id, cnt, key)
            replicatorAcks -= id
            replicatorAcks += ((id, (s, key, cnt, opTimer)))
          }
        case None =>
      }
    }
  }

  /* Behavior for the replica role. */
  val replica: Receive = {
    // Client protocol
    case Get(key, id) => {
      log.info("REPLICA secondary: Get(id={}): key = {} value={}", id, key, kv.get(key))
      sender ! GetResult(key, kv.get(key), id)
    }

    // Replicator protocol
    case Snapshot(key, _, seq) if seq < seqExpected => {
      log.info("REPLICA secondary: Snapshot(seq={} < expected={}): key = {}. Immediately ACK",
        seq, seqExpected, key)
      sender ! SnapshotAck(key, seq)
    }
    case Snapshot(key, _, seq) if seq > seqExpected => {
      log.info("REPLICA secondary: Snapshot(seq={} > expected={}): key = {}. Never ACK", seq, seqExpected, key)
      ()
    }
    case Snapshot(key, valueOption, seq) if seq == seqExpected => {
      log.info("REPLICA secondary: Snapshot(seq={} == expected={}): key = {} value = {}",
        seq, seqExpected, key, valueOption)
      // Check if we try to persist `seq` operation
      if (persistenceSecondaryAcks.contains(seq)) {
        log.info("REPLICA secondary: Snapshot(seq={}): Persistence is already running. Waiting...", seq)
      }
      else {
        log.info("REPLICA secondary: Snapshot(seq={}): Start Persistence.", seq)

        valueOption match {
          case Some(value) => kv += ((key, value))
          case None => kv -= key
        }
        // Persistence magic here
        val timer = doPersist(key, valueOption, seq)
        // Keep current operation and timer to control persistence protocol
        persistenceSecondaryAcks += ((seq, (sender, Persist(key, valueOption, seq), timer)))
      }
    }
    case PersistTimeout(seq) => {
      log.info("REPLICA secondary: PersistTimeout(seq={}).", seq)
      persistenceSecondaryAcks.get(seq) match {
        case Some((s, p, t)) =>
          log.info("REPLICA secondary: Resend Persist(seq={})", seq)
          // Stop timer
          t.cancel
          val timer = doPersist(p.key, p.valueOption, seq)
          // Keep current operation and timer to control replication protocol
          // TODO: how to update Map gracefully
          persistenceSecondaryAcks -= seq
          persistenceSecondaryAcks += ((seq, (s, Persist(p.key, p.valueOption, seq), timer)))
        case _ => log.info("REPLICA secondary: Race condition. Persist(seq={}) already processed", seq)
      }
    }
    case Persisted(key, seq) => {
      log.info("REPLICA secondary: Persisted(seq={}): key={}", seq, key)
      persistenceSecondaryAcks.get(seq) match {
        case Some((s, _, t)) =>
          // Stop timer
          t.cancel
          // Send finish to sender
          s ! SnapshotAck(key, seq)
          seqExpected = math.max(seqExpected, seq + 1L)
          persistenceSecondaryAcks -= seq
          log.info("REPLICA secondary: Persisted! Snapshot(seq={}) operation acknowledged for key={}", seq, key)
        case _ => log.info("REPLICA secondary: Race condition: seq={}", seq)
      }
    }
  }

  /* Persistence magic
   *
   * Return Cancellable Timer to control persistance protocol
   */
  def doPersist(key: String, valueOption: Option[String], id: Long): Cancellable ={
    // TODO: Actors timers https://doc.akka.io/docs/akka/current/actors.html#actors-timers
    persistence ! Persist(key, valueOption, id)
    context.system.scheduler.scheduleOnce(100.milliseconds, self, PersistTimeout(id))
  }
}

