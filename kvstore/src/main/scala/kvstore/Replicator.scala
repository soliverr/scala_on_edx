package kvstore

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Cancellable}
import akka.event.LoggingReceive

import scala.concurrent.duration._

/**
  * Replicator Actor.
  *
  * The role of this Replicator actor is to accept update events, and propagate the changes
  * to its corresponding replica (i.e. there is exactly one Replicator per secondary replica).
  */
object Replicator {
  /* Replicate(key, valueOption, id) is sent to the Replicator to initiate the replication
     of the given update to the key;
     in case of an Insert operation the valueOption will be Some(value)
     while in case of a Remove operation it will be None.

     The sender of the Replicate message shall be the Replica itself.
   */
  case class Replicate(key: String, valueOption: Option[String], id: Long)

  /* Replicated(key, id) is sent as a reply to the corresponding Replicate message
     once replication of that update has been successfully completed (see SnapshotAck).
     The sender of the Replicated message shall be the Replicator.
   */
  case class Replicated(key: String, id: Long)

  // Replcation protocol
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)
  // Enhanced replication protocol: restart snapshot
  case class SnapshotTimeout(seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor with ActorLogging {
  import Replicator._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  //  added Cancelable timer too
  var acks = Map.empty[Long, (ActorRef, Replicate, Cancellable)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq() = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  
  /* Behavior for the Replicator:
   *
   * Make sure that Replica keep right new key
   */
  def receive: Receive = LoggingReceive {
    case Replicate(key, valueOption, id) => {
      // TODO: batch magic here with pending
      val seq = nextSeq()
      log.info("REPLICATOR: Replicate(id={}): Start replication for key={}, seq={}", id, key, seq)
      log.info("REPLICATOR: Replicate(id={}):   from {}", id, sender)
      log.info("REPLICATOR: Replicate(id={}):     to {}", id, replica)
      replica ! Snapshot(key, valueOption, seq)
      val timer = context.system.scheduler.scheduleOnce(100.milliseconds, self, SnapshotTimeout(seq))
      // Keep current operation and timer to control replication protocol
      acks += ((seq, (sender, Replicate(key, valueOption, id), timer)))
    }
    case SnapshotAck(key, seq) => {
      log.info("REPLICATOR: SnapshotAck(seq={}): End replication for key={}", seq, key)
      acks.get(seq) match {
        case Some((s, r, t)) =>
          // Stop timer
          t.cancel
          // Send finish to Replica
          log.info("REPLICATOR: Send Replicated(id={}): key={} to {}", r.id, r.key, s)
          s ! Replicated(r.key, r.id)
          acks -= seq
        case _ => log.info("REPLICATOR: Race condition: seq={}", seq)
      }
    }
    case SnapshotTimeout(seq) => {
      log.info("REPLICATOR: SnaphotTimeout(seq={}).", seq)
      // TODO: batch magic here
      acks.get(seq) match {
        case Some((s, r, _)) =>
          log.info("REPLICATOR: Resend Snapshot(seq={})", seq)
          replica ! Snapshot(r.key, r.valueOption, seq)
          val timer = context.system.scheduler.scheduleOnce(100.milliseconds, self, SnapshotTimeout(seq))
          // Keep current operation and timer to control replication protocol
          acks -= seq
          acks += ((seq, (s, Replicate(r.key, r.valueOption, r.id), timer)))
        case _ => log.info("REPLICATOR: Race condition. Snapshot(seq={}) already processed", seq)
      }
    }
  }
}
