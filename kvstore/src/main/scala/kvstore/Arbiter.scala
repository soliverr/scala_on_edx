package kvstore

import akka.actor.{Actor, ActorRef, ActorLogging}
import akka.event.LoggingReceive

object Arbiter {
  case object Join

  case object JoinedPrimary
  case object JoinedSecondary

  /**
   * This message contains all replicas currently known to the arbiter, including the primary.
   */
  case class Replicas(replicas: Set[ActorRef])
}

class Arbiter extends Actor with ActorLogging {
  import Arbiter._
  var leader: Option[ActorRef] = None
  var replicas = Set.empty[ActorRef]

  def receive: Receive = LoggingReceive {
    case Join =>
      if (leader.isEmpty) {
        log.info("ARBITER: Join {} as PRIMARY", sender)
        leader = Some(sender)
        replicas += sender
        sender ! JoinedPrimary
      } else {
        log.info("ARBITER: Join {} as SECONDARY", sender)
        replicas += sender
        sender ! JoinedSecondary
      }
      leader foreach (_ ! Replicas(replicas))
  }

}
