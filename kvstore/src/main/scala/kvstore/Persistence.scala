package kvstore

import akka.actor.{Actor, ActorLogging, Props}
import akka.event.LoggingReceive

import scala.util.Random

object Persistence {
  case class Persist(key: String, valueOption: Option[String], id: Long)
  case class Persisted(key: String, id: Long)

  // Enhanced persistence protocol: restart persist operation
  case class PersistTimeout(seq: Long)

  class PersistenceException extends Exception("Persistence failure")

  def props(flaky: Boolean): Props = Props(classOf[Persistence], flaky)
}

class Persistence(flaky: Boolean) extends Actor with ActorLogging {
  import Persistence._

  def receive: Receive = LoggingReceive {
    case Persist(key, _, id) =>
      log.info("PERSISTENCE: Persist(id={}): key = {}", id, key)
      if (!flaky || Random.nextBoolean()) {
        log.info("PERSISTENCE: Persisted(id={})", id)
        sender ! Persisted(key, id)
      }
      else {
        log.info("PERSISTENCE: NOT Persisted(id={})", id)
        throw new PersistenceException
      }
  }

}
